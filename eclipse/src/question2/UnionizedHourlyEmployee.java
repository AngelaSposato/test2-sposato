//Angela Sposato 1934695
package question2;

//Extends HourlyEmployee, uses values from HourlyEmployee
public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double pensionContribution;
	//Constructor taking as input values from super class
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double pensionContribution) {
		//Calling constructor from HourlyEmployee
		super(hoursWorked, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	
	//Returns yearly pay by calling getYearlyPay from HourlyEmployee and adding pension contribution that unionized hourly employees receive but hourly employees do not
	public double getYearlyPay() {
		return super.getYearlyPay() + pensionContribution;
	}
}
