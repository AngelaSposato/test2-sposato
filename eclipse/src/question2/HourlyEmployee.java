//Angela Sposato 1934695
package question2;


public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;
	//Constructor 
	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}
	//Returns the yearly pay of an employee by multiplying their hours worked by their hourly pay and amount of weeks in a year
	public double getYearlyPay() {
		double yearlyPay = hourlyPay * hoursWorked * 52;
		return yearlyPay;
	}
	
}
