package question2;

//Angela Sposato 1934695

public class PayrollManagement {
	
	//This method tests HourlyEmployee, SalariedEmployee, UnionizedHourlyEmployee by creating array of type Employee
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee[] employees = new Employee[5];
		employees[0] = new HourlyEmployee(15, 13.25);
		employees[1] = new SalariedEmployee(50000);
		employees[2] = new UnionizedHourlyEmployee(20, 14.5, 2000);
		employees[3] = new HourlyEmployee(5, 13);
		employees[4] = new SalariedEmployee(100000);
		
		double totalExpense = getTotalExpenses(employees);
		System.out.println("Total expenses: " + totalExpense);
	}
	//Loops through all objects in array of type Employee and adds to a sum
	public static double getTotalExpenses(Employee[] employees) {
		double totalSalaries = 0;
		for (int i = 0; i<employees.length; i++) {
			totalSalaries = totalSalaries + employees[i].getYearlyPay();
		}
		return totalSalaries;
	}
}
