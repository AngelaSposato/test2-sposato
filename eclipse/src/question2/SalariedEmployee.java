//Angela Sposato 1934695
package question2;

//Class implements interface Employee
public class SalariedEmployee implements Employee {
	private double yearlySalary;
	//Constructor taking as input yearly salary
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	
	//returns yearly pay
	public double getYearlyPay() {
		return yearlySalary;
	}
}
