//Angela Sposato 1934695
package question4;
import question3.Planet;
import java.util.*;

public class CollectionMethods {
	//Method takes as input a Collection<Planet>, returns a Collection<Planet> where order is less than or equal to 3.
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		//Creating a new ArrayList because it a child of Collection
		Collection<Planet> innerPlanets = new ArrayList<Planet>(); 
		//Foreach loop that goes through planets
		for (Planet p: planets) {
			if (p.getOrder() <= 3) {
				//If Planet p's order is less than or equal to 3, add it to innerPlanets
				innerPlanets.add(p);
			}
		}
		return innerPlanets;
	}
}
