package question3;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	//Overriding equals method so that it checks equals on order and name
	@Override
	public boolean equals(Object o) {
		String thisName = this.name;
		int thisOrder = this.order;
		if (thisName.equals(((Planet)o).name) && thisOrder == ((Planet)o).order) {
			return true;
		}
		return false;
	}
	
	//Overriding hashCode() because I overwrote equals()
	@Override
	public int hashCode() {
		String combined = this.name + this.order;
		return combined.hashCode();
	}
	
	//Overriding compareTo so that it checks by name (increasing) and then by planetarySystemName (decreasing)
	@Override
	public int compareTo(Planet p) {
		int result = this.name.compareTo(p.getName());
		//If result is zero names are equal and needs to check by planetarySystemName
		if (result == 0) {
			int result2 = p.getPlanetarySystemName().compareTo(this.planetarySystemName);
			return result2;
		}
		return result;
	}
}
